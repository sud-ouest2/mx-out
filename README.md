# Configuration et scripts pour le/les serveurs MX sortant

L'infrastructure de sud-ouest2.org est prévue pour avoir plusieurs serveurx MX sortant, vous trouverez donc dans ce dépot les fichiers de configuration et / ou les scripts utilisés pour ça

Le principe est le suivant:
* La gestion administrative est gérée par Dolibarr
* Le système central est géré par Modoboa
* Un ou plusieurs serveurs SMTP sortant

Sur le serveur SMTP sortant
* Modoboa propose en standard plein de choses via des requêtes SQL -> on réutilise

## Installation OS

* OS de base : Debian GNU/Linux
* Paquets logiciels installés : voir le contenu du fichier dpkg.get_selections

## Configuration postfix
* Voir les fichiers présents dans le dépot

## Configuration DKIM
* Voir les fichiers présents dans le dépot
* Pour la configuration des domaines au niveau DKIM le choix a été fait d'utiliser une seule clé pour tous les domaines hébergés, ça sera peut-être / probablement un problème le jour où on devra changer de clé ... mais ça me semblait encore plus lourd de générer une clé différente pour chaque domaine
* mkdir -p /etc/opendkim/
* openssl genrsa -out /etc/opendkim/opendkim.key 2048 openssl rsa -in /etc/opendkim/opendkim.key -pubout -out /etc/opendkim/opendkim.pub.key chmod “u=rw,o=,g=” /etc/opendkim/opendkim.key
* chown opendkim:opendkim /etc/opendkim/opendkim.key
* Attention au bug debian9 https://bugs.debian.org/cgi-bin/bugreport.cgi?archive=no&bug=861169 -> /lib/opendkim/opendkim.service.generate systemctl daemon-reload service opendkim restart cf 

## Scripts personnalisés - DKIM et SPF
* Voir les fichiers /usr/local/bin/soo2-update_domains.sh et cron associé

## Certificats Let's encrypt pour postfix

Pour que postfix soit accessible en ssl/tls (trafic serveur-serveur) nous avons mis en place un certificat Let's Encrypt.

* Voir /usr/local/bin/soo2-update_certs.sh et son cron associé
* Voir la configuration main.cf de postfix smtp_tls et smtpd_tls
