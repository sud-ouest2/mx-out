#!/bin/bash
LOGFILE=/var/log/soo2/spamcop.log
SPAMALERT=spamcop@sud-ouest2.org
HOSTNAME=`hostname -f`
SPAMORIG="${2}"
SPAMDEST="${3}"

echo " =========== ${ladate} =========== " >> ${LOGFILE}
echo " spamcop called" >> ${LOGFILE}
#echo $@ >> ${LOGFILE}

# Localize these. The -G option does nothing before Postfix 2.3.
INSPECT_DIR=/tmp
SENDMAIL="/usr/sbin/sendmail -G -i" # NEVER NEVER NEVER use "-t" here.
# Exit codes from <sysexits.h>
EX_TEMPFAIL=75
EX_UNAVAILABLE=69

# Start processing.
cd $INSPECT_DIR || {
    echo "  $INSPECT_DIR does not exist" >> ${LOGFILE}
    exit $EX_TEMPFAIL;
}
cat >in.$$ || {
    echo "  Cannot save mail to file"  >> ${LOGFILE}
    exit $EX_TEMPFAIL;
}
# Clean up when done or when aborting.
trap "rm -f /tmp/in.$$" 0 1 2 3 15

# pour garder un log debug complet ...
# cat in.$$ >> ${LOGFILE}
#filter <in.$$ || {
#et le message d'erreur ...

rm -f /tmp/spamreport.$$
if [ "${SPAMORIG}" != "MAILER-DAEMON" ]; then
    HEADERFROM=`grep "^From:" in.$$`
    HEADERTO=`grep "^To:" in.$$`
    echo " ${SPAMORIG} to  ${SPAMDEST} " >> ${LOGFILE}
    echo "   ${HEADERFROM}" >> ${LOGFILE}
    echo "   ${HEADERTO}" >> ${LOGFILE}
    echo "Bonjour
L'utilisateur ${SPAMORIG} essaye d'envoyer du spam à destination de ${SPAMDEST} via notre infrastructure.

Plus précisément voici les entêtes From et To du mail (vous pouvez le trouver en pj) car il s'agit souvent d'alias ou d'adresse réécrites:
  * $HEADERFROM
  * $HEADERTO

Une copie de ce mail est disponible en piece-jointe (fichier au format gzip). Si vous voulez le consulter pensez à le décompresser.

Merci de faire le necessaire si cet utilisateur persévère. Ce spam a été refusé (mis à la poubelle).
--
envoye par soo2-spamcop.sh sur ${HOSTNAME} " > spamreport.$$
    trap "rm -f /tmp/spamreport.$$" 0 1 2 3 15

    gzip -k in.$$
    trap "rm -f /tmp/in.$$.gz" 0 1 2 3 15

    #mail -s "[soo2/spamalert] Attention ${SPAMORIG} envoie du spam vers ${SPAMDEST}" "${SPAMALERT}" -A in.$$.gz --content-type='text/plain; charset="utf-8"' < spamreport.$$
    echo "   run cat spamreport.$$ | /usr/bin/mutt -s [soo2/spamalert] Attention ${SPAMORIG} envoie du spam vers ${SPAMDEST} -a in.$$.gz -- ${SPAMALERT}" >> ${LOGFILE}
    (cat spamreport.$$ | /usr/bin/mutt -s "[soo2/spamalert] Attention ${SPAMORIG} envoie du spam vers ${SPAMDEST}" -a in.$$.gz -- "${SPAMALERT}")

    echo "This mail seems to be a spam, ${SPAMALERT} will be alerted ! Votre courrier électronique a été identifié comme un spam, ${SPAMALERT} en a été informé."

    logger "soo2-spamcop.sh : SPAM from ${SPAMORIG} destination ${SPAMDEST} deleted"
fi

rm -f "/tmp/in.$$.gz"
rm -f "/tmp/spamreport.$$"

#$SENDMAIL "$@" <in.$$
rm -f "/tmp/in.$$"
echo " the end" >> ${LOGFILE}
exit $?
