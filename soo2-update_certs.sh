#!/bin/bash
# Script Licence: GNU/GPL v3
#   2017 - Eric Seigne <eric.seigne@abuledu.org>
# automatic renew let's encrypt certificates
echo "======================= automatic renew let's encrypt certificates (start) ===================== "

# http-01 is used -> nginx help
echo "  starting nginx ..."
service nginx start

echo "  starting dehydrated ..."
dehydrated -c

echo "  stop nginx ..."
service nginx stop

echo "  reload postfix ..."
service postfix reload

echo "======================= automatic renew let's encrypt certificates (end) ===================== "
