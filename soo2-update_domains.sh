#!/bin/bash
PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin
#comme on est relais il faut qu'on sache quels sont les domaines virtuels qu'on heberge ...
#requete sur dolibarr pour avoir la liste ... https://sud-ouest2.org/dolibarr/public/soo/get_liste_domains.php

#serveur sympa dans notre infra
SYMPASRV="sympa-01.sud-ouest2.org"

FIC=`mktemp`
FICLISTES=`mktemp`

LADATE=`date "+%Y%m%d %H:%M"`
echo "------------- ${LADATE}"

echo "Fichier domaines temporaire : ${FIC}"
echo "Fichier listes temporaire :   ${FICLISTES}"

wget -q https://sud-ouest2.org/dolibarr/public/soo/get_liste_domains.php -O ${FIC}
if [ $? != 0 ]; then
    #erreur de transfert ou autre
    echo "wget (1) error"
    exit
fi

wget -q https://sud-ouest2.org/dolibarr/public/soo/get_liste_virtualhosts.php -O ${FICLISTES}
if [ $? != 0 ]; then
    #erreur de transfert ou autre
    echo "wget (2) error"
    exit
fi

#pour ne pas tout refaire si pas de nouveautes
REXIT=0
diff ${FIC} /etc/opendkim/reference
if [ $? == 0 ]; then
    #aucune modif -> exit
    echo "aucune modification de /etc/opendkim/reference -> exit code 1"
    REXIT=1
fi

LEXIT=0
diff ${FICLISTES} /etc/opendkim/referencelistes
if [ $? == 0 ]; then
    #aucune modif -> exit
    echo "aucune modification de /etc/opendkim/referencelistes -> exit code 2"
    LEXIT=1
fi

#si un des deux fichier est different on re genere tout
if [ ${LEXIT} == 0 -o ${REXIT} == 0 ]; then
    echo "LEXIT = ${LEXIT} et REXIT = ${REXIT} ..."
    cp /etc/opendkim/KeyTable.orig /etc/opendkim/KeyTable
    cp /etc/opendkim/SigningTable.orig /etc/opendkim/SigningTable
    cp /etc/postfix/transport_regexp_listes.orig /etc/postfix/transport_regexp_listes

    if [ -f ${FIC} ]; then
	cp ${FIC} /etc/opendkim/reference
    fi
    echo "les domaines:"
    for domaine in `cat ${FIC}`
    do
	echo "  $domaine"
	echo "$domaine		$domaine:dkim:/etc/opendkim/dkim.private" >> /etc/opendkim/KeyTable
	echo "*@$domaine		$domaine" >> /etc/opendkim/SigningTable
    done

    if [ -f ${FICLISTES} ]; then
	cp ${FICLISTES} /etc/opendkim/referencelistes
    fi
    echo "les listes:"
    for domaine in `cat /etc/opendkim/referencelistes`
    do
	echo "  liste $domaine"
	echo "$domaine		$domaine:dkim:/etc/opendkim/dkim.private" >> /etc/opendkim/KeyTable
	echo "*@$domaine		$domaine" >> /etc/opendkim/SigningTable
	#/.*@listes\..*/ smtp:${SYMPASRV}
	echo "/.*@${domaine}/ smtp:${SYMPASRV}" >> /etc/postfix/transport_regexp_listes
    done
        
    service postfix restart
    service opendkim restart
fi

rm -f ${FIC}
rm -f ${FICLISTES}

exit 0
